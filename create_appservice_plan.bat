az appservice plan create ^
--name %app_plan% ^
--resource-group %resource_group_name% ^
--is-linux ^
--location %resource_group_location% ^
--sku %app_sku% ^
--subscription %subscription_id%