rem - must be run from source directory

call az webapp up ^
--plan %app_plan% ^
--name %app_name% ^
--subscription %subscription_id% ^
--resource-group %resource_group_name% ^
--location %resource_group_location% ^
--sku %app_sku% ^
--verbose