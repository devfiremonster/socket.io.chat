
# Socket.IO Chat

A simple chat demo for socket.io

## Features

- Multiple users can join a chat room by each entering a unique username on website load.
- Users can type chat messages to the chat room.
- A notification is sent to all users when a user joins or leaves the chatroom.

## How to run locally

```
npm ci
npm start
```

browse: `http://localhost:3000`. Optionally, specify
a port by supplying the `PORT` env variable.

## How to run on Azure

Fill in all values in set_params.bat then run

```
set_params.bat
create_resource_group.bat
create_appservice_plan.bat
create_webapp.bat
deploy_webapp.bat
config_webapp.bat
```

browse: `https://{app_name}.azurewebsites.net`

## Debug

npm ls

https://{app_name}.scm.azurewebsites.net/
https://{app_name}.scm.azurewebsites.net/DebugConsole
https://{app_name}.scm.azurewebsites.net/dev/wwwroot/

https://{app_name}.scm.azurewebsites.net/api/vfs/LogFiles/{timestamp}_default_docker.log

Error: Cannot find module 'express'
- browse: https://{app_name}.scm.azurewebsites.net/
- click: Bash
- cd site/wwwroot
- note there is no node_modules folder, despite there being package-lock.json and package.json!
- npm ci
- restart webapp

## Verify

Verify sockets are being used by opening Fiddler and running the app. You will see entries like:
- GET /socket.io/?EIO=3&transport=websocket&sid=RLRoD99iZDPFU5q0AAAC HTTP/1.1.
- Connection: Upgrade

## Links

- [Socket.IO on Azure](https://docs.microsoft.com/en-us/azure/cloud-services/cloud-services-nodejs-chat-app-socketio)
- [Socket.IO Chat](https://github.com/socketio/socket.io/tree/master/examples/chat)
- [npm-ci](https://docs.npmjs.com/cli/v6/commands/npm-ci)