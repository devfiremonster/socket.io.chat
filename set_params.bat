@echo off

rem https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/set_1

rem remote stuff

set subscription_id=
set resource_group_name=
set resource_group_location=
set resource_group_tags=
set app_plan=
set app_sku=
set app_name=

echo subscription_id=%subscription_id%
echo resource_group_name=%resource_group_name%
echo resource_group_location=%resource_group_location%
echo resource_group_tags=%resource_group_tags%
echo app_plan=%app_plan%
echo app_sku=%app_sku%
echo app_name=%app_name%