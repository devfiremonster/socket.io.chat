call az webapp config set ^
--name %app_name% ^
--subscription %subscription_id% ^
--resource-group %resource_group_name% ^
--linux-fx-version "node|12-lts"

call az webapp config set ^
--name %app_name% ^
--subscription %subscription_id% ^
--resource-group %resource_group_name% ^
--web-sockets-enabled true

